import express from 'express';
import { DictionaryService } from '../services/dictionary-service';


export class DictionaryController {
    constructor(private dictionaryService: DictionaryService) { }

    searchWord = async (req: express.Request, res: express.Response) => {
        let searchWord = req.params.searchWord
        console.log('i am searching :', searchWord);
        if(!searchWord){
          res.status(400).json({ 'msg': 'Invalid search word.' });
          return
        }
        try {
            let receiveFromDB = await this.dictionaryService.getWordFromDB(searchWord);
            if (receiveFromDB) {
                console.log('search word is already exist in the database.');
                res.status(200).json(receiveFromDB);
            }else{
                let searchResult = await this.dictionaryService.searchFromAPI(searchWord);
                res.status(200).json(searchResult);
            }

        } catch (error) {
            res.status(500).json({ 'msg': 'error' });
            console.log(error);
        }
    }
}