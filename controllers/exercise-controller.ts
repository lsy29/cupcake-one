import express from 'express'
// import { Server } from 'socket.io'

// import { Memo } from '../models/memo'
import { ExerciseService } from '../services/exercise-service'

export class ExerciseController {
  constructor(private exerciseService: ExerciseService) {}

  htmlGetEx = async (req: express.Request, res: express.Response) => {

      console.log("req session user :", req.session['user'])

      let exId=req.session['video']
      console.log('exid=', exId)
      let exContent = await this.exerciseService.getExSql(exId) 
      console.log(req.session)
      // let temp = 'my name is '+ req.session['user'].username
      // console.log(temp)
      // console.log(exContent)
      res.json(exContent)
      
    
  }

  htmlGetInfo = async (req: express.Request, res: express.Response) => {
    let vdoId=req.session['video']
    let userId : any =req.session['user'].id

    let exInfo = await this.exerciseService.getInfoSql(vdoId,userId) 
    console.log(exInfo)
    // let temp = 'my name is '+ req.session['user'].username
    // console.log(temp)
    // console.log(exContent)
    res.json(exInfo)
    
  
}

  htmlPostAns = async (req: express.Request, res: express.Response) => {
    
    
    // console.log(typeof(req.body))
    // console.log(req.body)

    let userId : any =req.session['user'].id
    let exId : any = req.session['video'] // assume exID=videoID

    let ans1 = req.body.answer1;
    let ans2 = req.body.answer2;
    let ans3 = req.body.answer3;
    let ans4 = req.body.answer4;
    let ans5 = req.body.answer5;


    let myExPk = await this.exerciseService.postAnsSql(userId,exId,ans1,ans2,ans3,ans4,ans5)

    console.log('myExPk  :  ....',myExPk)

    res.redirect('./checkanswer.html')
      
    
  }

  htmlGetModelAns = async (req: express.Request, res: express.Response) => {
    let ansId=req.session['video'] // assume exID=videoID
    // console.log("from controller, ansId=", ansId);

    // let exId=req.session['video']
    let modelAns = await this.exerciseService.getModelAnsSql(ansId) 
    // console.log(req.session)
    // let temp = 'my name is '+ req.session['user'].username
    // console.log(temp)
    // console.log(modelAns)
    // req.session['video']=false
    // console.log(req.session)
    res.json(modelAns)
}


htmlGetYourAns = async (req: express.Request, res: express.Response) => {
  
  let userId : any = req.session['user'].id
  let ansId=req.session['video'] // assume exID=videoID
  // console.log("from controller, ansId=", ansId);

  // let exId=req.session['video']
  let yourAns = await this.exerciseService.getYourAnsSql(userId,ansId) 
  // console.log(req.session)
  // let temp = 'my name is '+ req.session['user'].username
  // console.log(temp)
  // console.log('yourAns::::' ,yourAns)
  req.session['video']=false
  // console.log(req.session)
  res.json(yourAns)
}


  
}
