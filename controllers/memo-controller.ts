import express from 'express'
import { Server } from 'socket.io'
import { MemoService } from '../services/memo-service'
import { Memo } from '../models/memo'

export class MemoController {
  constructor(private memoService: MemoService, private io: Server) {}

  createMemo = async (req: express.Request, res: express.Response) => {
    let content = req.body.content
    let filename = null
    if (req.file) {
      filename = req.file.filename
    }
    if (!content && !filename) {
      res.status(400).end('require content or image but both are not given')
      return
    }
    try {
      await this.memoService.createMemo({ content, filename })
      res.status(201).end('created')
      this.io.emit('new-memo')
    } catch (error) {
      console.log('failed to insert memo:', error)
      res.status(500).end('failed to insert memo')
    }
  }

  getAllMemos = async (req: express.Request, res: express.Response) => {
    try {
      let memos: Memo[] = await this.memoService.getAllMemos()
      res.json(memos)
    } catch (error) {
      console.log('failed to get memos:', error)
      res.status(500).end('failed to get memos')
    }
  }

  updateMemo = async (req: express.Request, res: express.Response) => {
    let id = parseInt(req.params.id)
    if (Number.isNaN(id)) {
      res.status(400).end('Expected numeric id in params')
      return
    }
    if (!req.body.content) {
      res.status(400).end('Missing content in req.body')
      return
    }
    try {
      let rowCount = await this.memoService.updateMemo({
        id,
        content: req.body.content,
      })
      if (rowCount === 0) {
        res.status(404).end('Memo not found')
      } else {
        res.status(202).end('Saved Memo')
        this.io.emit('updated-memo')
      }
    } catch (error) {
      console.log('failed to update memo:', { error, id })
      res.status(500).end('failed to update memo')
    }
  }

  deleteMemo = async (req: express.Request, res: express.Response) => {
    let id = parseInt(req.params['id'])
    if (Number.isNaN(id)) {
      res.status(400).end('expect number "id" in params')
      return
    }
    try {
      await this.memoService.deleteMemo(id)
      res.status(202).end('deleted')
      this.io.emit('deleted-memo', id)
    } catch (error) {
      console.error('failed to delete memo:', error)
      res.status(500).end('failed to delete memo')
    }
  }
}
