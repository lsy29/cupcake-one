import express from 'express';
// import { Server } from 'socket.io'
import { MyVocabService } from '../services/myVocab-service';
import { MyVocab } from '../models/myVocab';

export class MyVocabController {
    constructor(private myVocabService: MyVocabService) { }

    getWordFromDB = async (req: express.Request, res: express.Response) =>{
        let user_id : any = req.session['user'].id
        console.log('user_id session: ', user_id);
        

        let myVocabWord = req.body.myVocabWord
        // let myVocabWord = 'test'
        console.log('vocab word in controller: ', myVocabWord);
        if (!myVocabWord){
            console.log('miss vocab');
            res.status(400).end("Please include a vocab word")
            return
        }
        try{
            console.log('wait for DB');
            let receiveFromDB = await this.myVocabService.postResultToDB(myVocabWord, user_id);
            console.log('receive data from DB successfully. :',receiveFromDB);
            res.status(200).json(receiveFromDB);
            // console.log(receiveFromDB);
        }catch (error) {
            res.status(500).json({ 'msg': 'error' });
            console.log(error);
        }
    }

    getAllVocabs = async (req: express.Request, res: express.Response) => {
        try{
            let user_id : any = req.session['user'].id
            let vocabs: MyVocab[] = await this.myVocabService.getAllVocabs(user_id)
            res.json(vocabs)
        }catch (error) {
            console.log('failed to get memos: ', error);
            res.status(500).end('failed to get vocabs')
        }
    }

    deleteVocab = async (req: express.Request, res: express.Response) => {
        let id = parseInt(req.params['id'])
        if (Number.isNaN(id)) {
            res.status(400).end('expect number "id" in params')
            return
        }
        try{
            await this.myVocabService.deleteVocab(id)
            res.status(202).end('deleted')
            // this.io.emit('deleted-vocab', id)
        }catch (error){
            console.error('failed to delete vocab: ', error);
            res.status(500).end('failed to delete vocab')
        }
    }
   

    // createMyVocabWord = async (req: express.Resquest, res: express.Response)=>{
    //     let content = await this.myVocabService.getWordFromDB()
    //     res.json(content)
    // }
}