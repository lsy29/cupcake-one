import express from 'express';
import SubtitleService from '../services/subtitle-service'

class SubtitleController {
    constructor(private service: SubtitleService) { }

    getSubtile = async (req: express.Request, res: express.Response) => {
        //trying session
        // req.session['video']= 3
        
        console.log("@@@@@@@@@@@ req session @@@@@@@@@@@ :", req.session)
        console.log(req.query)
        if(5>10){
            console.log(this.service);
        }

        //let videoId : any = req.session['video']
        let videoId : any = req.query.vid
        req.session['video']=videoId

        console.log("req.session['video']=", videoId)
        let subtitle= await this.service.getSubtitle(videoId); 
        console.log('subtitle=', subtitle)

        res.json(subtitle)
    }


}

export = SubtitleController

