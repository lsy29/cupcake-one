import { UserService } from '../services/user-service'
import express from 'express'
import { checkPassword } from '../hash'

export class UserController {
  constructor(private userService: UserService) { }

  loginWithGoogle = async (req: express.Request, res: express.Response) => {
    const accessToken = req.session?.['grant'].response.access_token
    if (!accessToken) {
      res.status(400).end('missing accessToken')
      return
    }
    let user = await this.userService.createOrUpdateGoogleUser(accessToken)
    req.session['user'] = user
    res.redirect('/')
  }




  register = async (req: express.Request, res: express.Response) => {
    let username = req.body.username
    if (!username) {
      res.status(400).end('missing username')
      return
    }
    let password = req.body.password
    if (!password) {
      res.status(400).end('missing password')
      return
    }
    
    let result = await this.userService.createUser(username , password, 'admin')
    if (result) {
      res.status(201).end('Created User')
    } else {
      console.log(result)
      res.status(500).end('Failed to create user')
    }
  }




  login = async (req: express.Request, res: express.Response) => {
    let user = await this.userService.getUserByUserName(req.body.username)
    if (!user) {
      res.status(403).end('wrong username')
      return
    }
    if (!user.password) {
      res.status(403).end(`User didn't signup with password`)
      return
    }
    let matched = await checkPassword(req.body.password, user.password)
    if (!matched) {
      res.status(403).end('wrong username or password')
      return
    }
    delete user.password
    console.log(req.session)
    req.session['user'] = user
    console.log(req.session)
    //testing
    // res.end('Login Success')

  //  req.session['video'] = false
    if (!req.session['video']) { //if no selected video, ie. initial stage for that browser
      res.status(200).end('Login Success Before Video Selection')
    } else  {  // remind user to log in after he/she selected a specific video
      console.log("req.session['video'] != false")
      // res.redirect('./exercise.html')
      res.status(200).end(req.session['video'])
    }
    
   
    
    // res.redirect('/public/exercise.html')
    // res.json('Login Success')

  }




  logout = (req: express.Request, res: express.Response) => {
    req.session['user'] = false
    req.session['grant'] = false
    console.log(req.session)
    res.status(200).end()
  }

  

  

  getRole = (req: express.Request, res: express.Response) => {
//testing
    // req.session['video'] = false // **** be careful
    // console.log("in getRole: req.session['video'] = false",)


    if (!req.session || !req.session['user']) {
      res.end('guest')
      return
    }
    if (req.session['user'].role === 'admin') {
      res.end('admin')
      return
    }
    res.end('normal')
  }


  // getUserProfile = (req: express.Request, res: express.Response) => {
  //   if (req.session['user'] == 'admin') {
  //     res.end ('admin')
  //     return
  //   }
  //   res.end('guest')
  // }

  
  


  



  
  getUserProfile = async (req: express.Request, res: express.Response) => {
    if (!req.session["user"]) {
    res.json({});
    return;
  } else {    
    let user = await req.session["user"];
    res.json(user);
    return;
  }


  
}


}
