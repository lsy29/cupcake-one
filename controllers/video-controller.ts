
import express from 'express';
import VideoService from '../services/video-service'
import {videoCat} from'../videoCat';

class VideoController {
    constructor(private service: VideoService) { }

    //testing only
    getTitleAndNumber = async (req: express.Request, res: express.Response) => {
        //trying session
        
        console.log(req.query)
        if(5>10){
            console.log(this.service);
        }
        let catId : any = req.query.cat

        let catWord=videoCat[catId] //["BBC", "Netflix", "Disney", "Health", "Songs", "Science"]
        console.log(catWord)
        // let titleAndNumber= await this.service.getTitleAndNumber("BBC"); 
        let titleAndNumber= await this.service.getTitleAndNumber(catWord); 

        console.log('subtitle=', titleAndNumber)

        res.json(titleAndNumber)
    }

}

export = VideoController




