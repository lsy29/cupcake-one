import Knex from 'knex'
import { env } from './env';

let knexConfigs = require('./knexfile')
let mode = env.NODE_ENV
console.log({mode})
export const knex = Knex(knexConfigs[mode])