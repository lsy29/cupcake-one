import express from 'express';
// import {Request,Response} from 'express';
import multer from "multer";
import axios from 'axios';
// import fs from 'fs';
// import path from 'path';
import dotenv from "dotenv";
// import Knex from 'knex';

// let searchWord;



const app = express();
// const knexConfigs = require('./knexfile');
// const configMode = process.env.NODE_ENV || "development";
// const knexConfig = knexConfigs[configMode]
// const knex = Knex(knexConfig)

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    let filename = file.originalname;
    cb(null, filename);
  },
});

  dotenv.config();

const upload = multer({ storage });

console.log(upload);

app.use(express.static('public'));



app.get('/search/:searchWord', async (req, res) => {
  let searchWord = req.params.searchWord
  console.log('i am searching :', searchWord);
  if(!searchWord){
    res.json({ 'msg': 'Invalid search word.' });
    return
  }
  let wordResult: any = ''
  axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en_US/${searchWord}`)
    .then(function (response) {
      wordResult = response.data
      res.json(wordResult);
    })
    .catch(function (error) {
      console.log(error);
      res.json({ 'msg': 'error' });
    })
    .finally(function () {
      // always executed
    });
})



const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});

// data need to be url encoded





//knex join note
            // .select("*")
            // .from('dictionary')
            // .join('my_vocab', 'my_vocab.dictionary_id', '=', 'dictionary.id')
            // .where({word: myVocabWord})

            // select * from dictionary join my_vocab on my_vocab.dictionary.id = dictionary.id