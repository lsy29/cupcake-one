import grant from 'grant'
import { createHttpTerminator } from 'http-terminator'
import express from 'express'
import util from 'util'
import fs from 'fs'
import expressExpress from 'express-session'
import path from 'path'
import { format } from 'fecha'
import dotenv from 'dotenv'
import http from 'http'
import socketIO from 'socket.io'
import { env } from './env'
import { setSocketIO } from './socketio'
// import formatMessage from './public/chat/utils/messages'
// import {userJoin,
//   getCurrentUser,
//   userLeave,
//   getRoomUsers} from './public/chat/utils/users'
import { knex } from './db'
import { isLoggedIn } from './guards'
import { createRouter } from './routes'
// import multer from 'multer'

import { UserService } from './services/user-service'
import { UserController } from './controllers/user-controller'
import SubtitleController from './controllers/subtitle-controller'
import SubtitleService from './services/subtitle-service'
import VideoController from './controllers/video-controller'
import VideoService from './services/video-service'
import { DictionaryController } from './controllers/dictionary-controller'
import { DictionaryService } from './services/dictionary-service'
import { MyVocabService } from './services/myVocab-service'
import { MyVocabController } from './controllers/myVocab-controller'


import { ExerciseController } from './controllers/exercise-controller'
import { ExerciseService } from './services/exercise-service'




dotenv.config()

let app = express()
let server = http.createServer(app)
let io = new socketIO.Server(server)
setSocketIO(io)

const httpTerminator = createHttpTerminator({ server })

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

let sessionMiddleware = expressExpress({
  secret: 'cupcake idea from cake', // cupcake graffiti !!! 
  saveUninitialized: true,
  resave: true,
})
app.use(sessionMiddleware)

const grantExpress = grant.express({
  defaults: {
    origin: `http://ec2-3-23-231-30.us-east-2.compute.amazonaws.com`,
    // origin: `http://${env.HOST}:${env.PORT}`,
    transport: 'session',
    state: true,
  },
  google: {
    key: env.GOOGLE_CLIENT_ID || '',
    secret: env.GOOGLE_CLIENT_SECRET || '',
    scope: ['profile', 'email'],
    callback: '/login/google',
  },
})

app.use(grantExpress as express.RequestHandler)

app.use((req:any, res:any, next:any) => {
  let counter = req.session['counter'] || 0
  counter++
  req.session['counter'] = counter
  // console.log(req.session)
  // console.log(counter);
  next()
})

app.use((req, res, next) => {
  let date = new Date()
  let text = format(date, 'YYYY-MM-DD hh:mm:ss')
  console.log(`[${text}]`, req.method, req.url)
  next()
})

// app.use(express.static('uploads'))
app.use('/admin', isLoggedIn, express.static('protected'))

// let storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, './uploads')
//   },
//   filename: (req, file, cb) => {
//     let filename = file.fieldname + '-' + Date.now() + '-' + file.originalname
//     cb(null, filename)
//   },
// })
// let upload = multer({ storage })

let subtitleService = new SubtitleService(knex)
let userService = new UserService(knex)
let dictionaryService = new DictionaryService(knex)
let exerciseService = new ExerciseService(knex)
let videoService = new VideoService(knex)
let myVocabService = new MyVocabService(knex)
// knex.insert

let subtitleController = new SubtitleController(subtitleService)
let userController = new UserController(userService)
let dictionaryController = new DictionaryController(dictionaryService)
let exerciseController = new ExerciseController(exerciseService)
let videoController = new VideoController(videoService)
let myVocabController = new MyVocabController(myVocabService)



let router = createRouter({
  isLoggedIn,
  subtitleController,
  videoController,
  // userController
  // memoController,
  userController,
  dictionaryController,
  exerciseController,
  myVocabController
})
app.use(router)

app.use(express.static('public'))




app.use((req:any, res:any) => {
  res.sendFile(path.join(__dirname, 'public', '404.html'))
})

export async function main(PORT: number) {
  if (!fs.existsSync('uploads')) {
    fs.mkdirSync('uploads')
  }
  await new Promise<void>((resolve, reject) => {
    server.listen(PORT, () => {
      console.log(`listening on http://localhost:${PORT}`)
      
      resolve()
    })
    server.on('error', reject)
  })
  async function close() {
    console.debug('close io server')
    await util.promisify(io.close.bind(io))()

    console.debug('close knex clint')
    await knex.destroy()

    console.debug('close http server')
    await httpTerminator.terminate()

    console.debug('closed all')
  }
  return { server, close }
}
