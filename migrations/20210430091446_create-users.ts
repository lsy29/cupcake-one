import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if(!hasTable){
        await knex.schema.createTable('users', (table)=>{
            table.increments();
            table.string("username", 40);
            table.string("password", 255);
            table.string("email", 40);
            table.string('google_access_token', 255);
            table.string("role", 20);
            table.string("level", 20);
            table.integer("score");
            table.integer("coins");
            table.timestamps(false,true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("users");
}