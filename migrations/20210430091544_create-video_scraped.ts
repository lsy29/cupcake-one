import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('video_scraped');
    if(!hasTable){
        await knex.schema.createTable('video_scraped', (table)=>{
            table.increments();
            table.string('video_title', 25);
            table.jsonb('scrap_content').notNullable(); // video_id from here
            table.string('video_genre', 25).nullable();
            table.string('length', 25);
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("video_scraped");
}

