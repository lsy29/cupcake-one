import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('video_clicked');
    if(!hasTable){
        await knex.schema.createTable('video_clicked', (table)=>{
            table.increments();
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.integer('video_id').unsigned();
            table.foreign('video_id').references('video_scraped.id');
            table.string('video_views', 40);
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('video_clicked');
}

