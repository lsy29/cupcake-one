import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('subtitle');
    if(!hasTable){
        await knex.schema.createTable('subtitle', (table)=>{
            table.increments();
            table.integer('video_id').unsigned();
            table.foreign('video_id').references('video_scraped.id');
            table.jsonb('subtitle').notNullable();  // subtitle scraped content here
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('sutitle');
}

