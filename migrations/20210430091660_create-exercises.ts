import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('exercises');
    if(!hasTable){
        await knex.schema.createTable('exercises', (table)=>{
            table.increments();
            table.integer('dictionary_id').unsigned(); // drop
            table.foreign('dictionary_id').references('dictionary.id'); // drop
            table.integer('video_id').unsigned();
            table.foreign('video_id').references('video_scraped.id');
            table.integer('subtitle').unsigned();
            table.foreign('subtitle').references('subtitle.id');
            table.jsonb('content').notNullable();   // exercises content here
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('exercises');
}

