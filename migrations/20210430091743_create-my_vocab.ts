import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('my_vocab');
    if(!hasTable){
        await knex.schema.createTable('my_vocab', (table)=>{
            table.increments();
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.integer('dictionary_id').unsigned();
            table.foreign('dictionary_id').references('dictionary.id');
            table.timestamps(false,true);
        })
    }
    
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('my_vocab');
}

