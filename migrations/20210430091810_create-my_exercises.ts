import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('my_exercises');
    if(!hasTable){
        await knex.schema.createTable('my_exercises', (table)=>{
            table.increments();
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.integer('exercises_id').unsigned();
            table.foreign('exercises_id').references('exercises.id');
            table.integer('attempt');
            table.integer('highest_score');
            table.timestamps(false, true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('my_exercises');
}

