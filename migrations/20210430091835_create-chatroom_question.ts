import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('chatroom_question');
    if(!hasTable){
        await knex.schema.createTable('chatroom_question', (table)=>{
            table.increments();
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.boolean('stared');
            table.text('content').notNullable();
            table.timestamps(false, true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('chatroom_question');
}


