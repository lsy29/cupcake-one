import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('my_contribution');
    if(!hasTable){
        await knex.schema.createTable('my_contribution', (table)=>{
            table.increments();
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.integer('question_id').unsigned();
            table.foreign('question_id').references('chatroom_question.id');
            table.integer('answer_id').unsigned();
            table.foreign('answer_id').references('chatroom_answer.id');
            table.timestamps(false, true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('my_contribution');
}

