import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('video_scraped', (table)=>{
        table.string('youtube_video_id', 40).nullable();
        table.string('video_url', 255).nullable();
        table.string('language', 30).nullable();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('video_scraped', (table)=>{
        table.dropColumn('youtube_video_id');
        table.dropColumn('video_url');
        table.dropColumn('language');
    })
}

