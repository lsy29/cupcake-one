import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('video_scraped', (table)=>{
        table.dropColumn('scrap_content');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('video_scraped', (table)=>{
        table.jsonb('scrap_content');
    })
}

