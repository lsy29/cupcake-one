import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('dictionary', (table)=>{
        table.dropColumn('scraped_content');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('dictionary', (table)=>{
        table.jsonb('scraped_content').notNullable();
    })
}

