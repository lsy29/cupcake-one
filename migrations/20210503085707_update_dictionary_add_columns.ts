import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('dictionary', (table)=>{
        table.string('word', 100);
        table.string('phonetics', 255);
        table.string('audio', 255);
        table.string('meanings', 255);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('dictionary', (table)=>{
        table.dropColumn('word');
        table.dropColumn('phonetics');
        table.dropColumn('audio');
        table.dropColumn('meanings');
    })
}

