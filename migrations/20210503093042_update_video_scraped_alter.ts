import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('video_scraped', (table)=>{
        table.string('length', 255).alter();
        table.string('video_title', 255).alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('video_scraped', (table)=>{
        table.string('length', 25).alter();
        table.string('video_title', 25).alter();
    })
}

