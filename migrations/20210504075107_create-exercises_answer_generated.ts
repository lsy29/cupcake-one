import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('exercises_answer_generated');
    if (!hasTable){
        await knex.schema.createTable('exercises_answer_generated', (table)=>{
            table.increments();
            table.integer('ex_id').unsigned();
            table.foreign('ex_id').references('exercises.id');
            table.string('model_answer1', 255);
            table.string('model_answer2', 255);
            table.string('model_answer3', 255);
            table.string('model_answer4', 255);
            table.string('model_answer5', 255);
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('exercises_answer_generated');
}

