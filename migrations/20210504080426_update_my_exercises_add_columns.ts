import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('my_exercises', (table)=>{
        table.string('posted_answer1', 255);
        table.string('posted_answer2', 255);
        table.string('posted_answer3', 255);
        table.string('posted_answer4', 255);
        table.string('posted_answer5', 255);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('my_exercises', (table)=>{
        table.dropColumn('posted_answer1');
        table.dropColumn('posted_answer2');
        table.dropColumn('posted_answer3');
        table.dropColumn('posted_answer4');
        table.dropColumn('posted_answer5');
    })
}

