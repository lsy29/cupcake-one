import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('dictionary', (table)=>{
        table.string('partOfSpeech');
        table.string('definition');
        table.string('example');
        table.string('synonyms');        
        table.string('antonym');        
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('dictionary', (table)=>{
        table.dropColumn('partOfSpeech');
        table.dropColumn('definition');
        table.dropColumn('example');
        table.dropColumn('synonyms');
        table.dropColumn('antonym');
    })
}

