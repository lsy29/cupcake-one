import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('dictionary', (table)=>{
        table.json('phonetics');
        table.json('meanings');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('dictionary', (table)=>{
        table.dropColumn('phonetics');
        table.dropColumn('meanings');
    })
}

