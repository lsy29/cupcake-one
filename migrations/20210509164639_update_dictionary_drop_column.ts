import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('dictionary', (table)=>{
        table.dropColumn('user_id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('dictionary', (table)=>{
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('users.id');
    })
}

