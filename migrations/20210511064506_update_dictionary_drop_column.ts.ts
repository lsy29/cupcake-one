import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('dictionary', (table)=>{
        table.dropColumn('audio');
        table.dropColumn('partOfSpeech');
        table.dropColumn('definition');
        table.dropColumn('example');
        table.dropColumn('synonyms');
        table.dropColumn('antonym');
        table.dropColumn('text');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('dictionary', (table)=>{
        table.string('audio', 255);
        table.string('partOfSpeech');
        table.string('definition');
        table.string('example');
        table.string('synonyms');        
        table.string('antonym');
        table.string('text');
    })
}

