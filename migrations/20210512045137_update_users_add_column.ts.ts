import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('users', (table)=>{
        table.string('user_icon');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', (table)=>{
        table.dropColumn('users_icon');
    })
}

