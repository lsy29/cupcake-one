export type Dictionary = {
    id: number
    word: String
    text: String
    audio: String
    partOfSpeech: String
    definition: String
    example: String
    synonyms?: String  // nullable()
    antonym?: String  // nullable()
}
