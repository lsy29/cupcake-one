export type Memo = {
  id: number
  content: string
  image?: string
}