export type MyVocab = {
    id: number
    word: string
    phonetics: JSON
    meanings: JSON
  }