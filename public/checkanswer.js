

let exDiv=document.querySelector('#exercise-display')


async function loadEx(){

    let res = await fetch('/GETEXERCISE')

    let temp= await res.json()


    console.log(typeof(temp))
    console.log(temp[0].content)

    exDiv.innerHTML = temp[0].content

}

loadEx()

let modelAnsDiv=document.querySelector('#displaymodelfive')
let yourAnsDiv=document.querySelector('#displayyourfive')

let pointsDiv=document.querySelector('#exercisepoints')
let highestDiv=document.querySelector('#highestpoints')



async function showAnswer(){

    let res1 = await fetch('/getmodelanswer')
    let temp1= await res1.json()




    // console.log(typeof(temp))
    // console.log(temp[0].model_answer1)

    modelAnsDiv.innerHTML =
    '1.  ' + temp1[0].model_answer1 + '<br>' +
    '2.  ' + temp1[0].model_answer2 + '<br>' +
    '3.  ' + temp1[0].model_answer3 + '<br>' +
    '4.  ' + temp1[0].model_answer4 + '<br>' +
    '5.  ' + temp1[0].model_answer5 + '<br>' 
////

let res2 = await fetch('/getyouranswer')
let temp2= await res2.json()

    yourAnsDiv.innerHTML =
    '1.  ' + temp2[0].posted_answer1 + '<br>' +
    '2.  ' + temp2[0].posted_answer2 + '<br>' +
    '3.  ' + temp2[0].posted_answer3 + '<br>' +
    '4.  ' + temp2[0].posted_answer4 + '<br>' +
    '5.  ' + temp2[0].posted_answer5 + '<br>'

////

pointsDiv.innerHTML = temp2[0].attempt

highestDiv.innerHTML = temp2[0].highest_score


}

showAnswer()


let username=document.querySelector('#currentusername')
let utubeDiv=document.querySelector('#youtubediv')




async function loadInfo(){

    let res = await fetch('/getexinfo')

    let temp= await res.json()

    console.log(temp[0][0].username)
    utube_vdeid=temp[1][0].youtube_video_id
    console.log(utube_vdeid)
    utube_url=temp[2][0].video_url
    console.log(utube_url)

    username.innerHTML = temp[0][0].username

    utubeDiv.innerHTML = `
  
    <a href="#" >
    <iframe width="300" height="200"  src="https://www.youtube.com/embed/${utube_vdeid}"
        title="YouTube video player" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen></iframe>
    </a>
    
    `

}


loadInfo()


async function checkRole() {
    await fetch("/getUserProfile").then(async (res) => {
      let user = await res.json();
      console.log('user =', user)
      console.log('icon = ', user.user_icon)
      if (user) {
        //<i class="fas fa-user-circle"></i
        if(user.email){
          
          document.querySelector("#currentusername").innerHTML=` <${user.username}/>`
          document.querySelector("#image").src = user.user_icon
          console.log('22222')
  
        }
        else {
          document.querySelector("#image").src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"
  
        }
       
      }
      
    });
  }
  checkRole()
