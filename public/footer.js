let footer = document.querySelector("footer");
footer.innerHTML = /* html */ `




<footer class="bg-dark text-center text-white" id="contact">
        <!-- Grid container -->
        <div class="container p-4">
            <!-- Section: Social media -->
            <section class="mb-4">
                <!-- Facebook -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-facebook-f"></i></a>

                <!-- Twitter -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-twitter"></i></a>

                <!-- Google -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-google"></i></a>

                <!-- Instagram -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-instagram"></i></a>

                <!-- Linkedin -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-linkedin-in"></i></a>

                <!-- Github -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-github"></i></a>
            </section>
            <!-- Section: Social media -->



            <!-- Section: Text -->
            <section class="mb-4">
                <p>
                    We build CUPCAKE to help people learn english in a fun and easy way
                </p>
            </section>
            <!-- Section: Text -->


            <!-- Grid container -->

            <!-- Copyright -->
            <div class="text-center p-3">
                © 2021 Copyright:
                <a class="text-white" href="https://cupcake.com/">cupcake.com</a>
            </div>
            <!-- Copyright -->
    </footer>


`;