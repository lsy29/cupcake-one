const popup = document.querySelector('#dict-popup');
const dictIcon = document.querySelector('.fa-atlas');
const dictText = document.querySelector('.dict-text');
const chatBtn = document.querySelector('.chat-btn');
const submitBtn = document.querySelector('.submit');
const dictSearchBtn = document.querySelector('#dict-search');
const chatArea = document.querySelector('.chat-area');
const inputElm = document.querySelector('input');
// import { loadMyVocab } from './notebook-myVocab.js';


      

//   chat button toggler 

chatBtn.addEventListener('click', ()=>{
    popup.classList.toggle('show');
})

dictSearchBtn.addEventListener('click', async event =>{
  event.preventDefault()
  dictIcon.classList.add('.dict-hidden');
  dictText.classList.add('.dict-hidden');
  const searchWord = document.querySelector("#search").searchWord.value
  // const searchWord = 'test'
  console.log("earchWord: ", searchWord);
    if (!searchWord){
      return 
    }
    const res = await fetch(`/search/${searchWord}`)
    console.log("fetch sucess");
    const result = await res.json()
    const resultDiv = document.querySelector('#dict-result')
    resultDiv.innerHTML = ""
    // meaningArray = result[0].meanings
    resultWord = `<div id='searchWord' style=" font-size: 30px"><b>${result.word}</b></div>`
    phoneticsArray = result.phonetics
    console.log('result: ', result);
    meaningArray = result.meanings

    let textHTML = ''
    for (let text of phoneticsArray){
      let ResultText = text.text
      textHTML += `
      <div>[${ResultText}]</div>
      `
    }
    let audioHTML = ''
    for (let audio of phoneticsArray){
      let ResultAudio = audio.audio
      console.log('ResultAudio: ', ResultAudio);
      audioHTML += `
      <button onclick='playMelodia("melodia${audio.text}")'>
        <i class="fas fa-volume-down"></i>
      </button>
      <audio id="melodia${audio.text}" controls="" style="display: none"><source src="${ResultAudio}" type="audio/mp3"></audio>
      `
    }

    console.log('meaningArray: ', meaningArray);
    let meaningHTML = ''
    let partOfSpeechHTML = ''
    let ResultDictionary = ''
    for (let meaning of meaningArray){
        let definitionArray = meaning.definitions
        // let exampleArray = meaning.example
        let partOfSpeech = meaning
        let ResultPartOfSpeech = partOfSpeech.partOfSpeech
        // if(ResultPartOfSpeech == 'transitive verb'){
        //   ResultPartOfSpeech = 'v.t.';
        // }else if(ResultPartOfSpeech == 'transitive verb'){

        // }
        let partOfSpeechSplit = ResultPartOfSpeech.split(/\s/).reduce((response,word)=> response+=word.slice(0,1),'')
        let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.')
        // partOfSpeechHTML += `<p font-weight: bold>${ResultPartOfSpeech}</p>`
        for (let definition of definitionArray){
            console.log('definition = ', definition);
            ResultDictionary = definition.definition
            console.log(typeof(result));

            let ResultExample = definition.example
            if (ResultExample === undefined){
                console.log();
                ResultExample = `<span style=" color: gray"><i>Not Available</i></span>`
                meaningHTML += `
                <div class="card">
                    <div class="container">
                    <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                    <p></p>
                    <div>Definition: </div>
                    <div>${ResultDictionary}</div>
                    <p></p>
                    <div>Example: </div><br>
                    <div>${ResultExample}</div>
                    </div>
                </div>`
            }else {
                meaningHTML += `
                <div class="card">
                    <div class="container">
                    <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                    <p></p>
                    <div>Definition: </div>
                    <div>${ResultDictionary}</div>
                    <p></p>
                    <div>Example: </div><br>
                    <div>${ResultExample}</div>
                    </div>
                </div>`
            }
        }


    }
    // meaningHTML += '</ul>'
    resultDiv.innerHTML = resultWord + textHTML + audioHTML + partOfSpeechHTML + meaningHTML
})

// send msg 
// submitBtn.addEventListener('click', ()=>{
//     let userInput = inputElm.value;

//     let temp = `<div class="out-msg">
//     <span class="my-msg">${userInput}</span>
//     <img src="img/me.jpg" class="avatar">
//     </div>`;

//     chatArea.insertAdjacentHTML("beforeend", temp);
//     inputElm.value = '';

// }) 

async function addMyVocab(myVocabWord) {
  myVocabWord = document.querySelector('#searchWord').value;
  console.log("myVocabWord: ", myVocabWord);
  let res = await fetch('/myVocab', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          myVocabWord: myVocabWord,
      }),
  })
  console.log('fetch successfully');
  console.log('res.body: ', await res.json());
  loadMyVocab()
}


function playMelodia(id) {
  document.getElementById(id).play();
}




  async function logout() {
    let res = await fetch("/logout", { method: "POST" });
    checkRole();
    window.location.href = "/";
  }
  
  
  

  async function checkRole() {
  let res = await fetch('/role')
  let role = await res.text()
  document.body.classList.add(role)
}
checkRole()
  




 






