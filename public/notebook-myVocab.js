let resultDivConSub = document.querySelector('#vocab-card-controller')
let resultDivSub = document.querySelector('#vocab-card')

async function loadMyVocab() {
    console.log('loadMyVocab function running');
    let res = await fetch('/getMyVocab')
    const result = await res.json()
    let resultCard = resultDivSub.innerHTML
    resultCard = ''
    // for (let eachResult of result) {
    //     console.log('eachResult: ', eachResult);
    for (let i = 0; i < result.length; i++) {
        console.log('result.length: ', result.length);
        // for (let vocab of result){
        let resultArray = result[i]
        resultWord = /* HTML*/ `<div id="searchWordSub" style=" font-size: 30px"><b>${resultArray.word}</b></div>`
        console.log('result: ', resultArray);
        phoneticsArray = resultArray.phonetics
        console.log('phoneticsArray: ', phoneticsArray);
        meaningArray = resultArray.meanings
        let textHTML = ''
        // for (let i = 0; i < result.length; i++) {
            for (let text of phoneticsArray) {
                let ResultText = text.text
                textHTML += `
              <div>[${ResultText}]</div>
              `
            }
            let audioHTML = ''
            for (let audio of phoneticsArray) {
                let ResultAudio = phoneticsArray[0].audio
                console.log('ResultAudio: ', ResultAudio);
                audioHTML += `
              <button onclick='playMelodia("melodia${audio.text}")'>
                <i class="fas fa-volume-down"></i>
              </button>
              <audio id="melodia${audio.text}" controls="" style="display: none"><source src="${ResultAudio}" type="audio/mp3"></audio>
              `
            }

            console.log('meaningArray: ', meaningArray);
            let meaningHTML = ''
            let partOfSpeechHTML = ''
            for (let meaning of meaningArray) {
                let definitionArray = meaning.definitions
                // let exampleArray = meaning.example
                let partOfSpeech = meaning
                let ResultPartOfSpeech = partOfSpeech.partOfSpeech
                let partOfSpeechSplit = ResultPartOfSpeech.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '')
                let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.')
                // partOfSpeechHTML += `<p font-weight: bold>${ResultPartOfSpeech}</p>`
                for (let definition of definitionArray) {
                    console.log('definition = ', definition);
                    let ResultDictionary = definition.definition
                    console.log((result[0]));
                    let ResultExample = definition.example

                    if (ResultExample === undefined) {
                        ResultExample = `<span style=" color: gray"><i>Not Available</i></span>`
                        meaningHTML += `
                        <div class="card" id='vocab-${result[i].id}'>
                            <div class="container">
                            <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                            <p></p>
                            <div>Definition: </div>
                            <div>${ResultDictionary}</div>
                            <p></p>
                            <div>Example: </div><br>
                            <div>${ResultExample}</div>
                            </div>
                        </div>`
                    } else {
                        meaningHTML += `
                            <div class="card">
                                <div class="container">
                                <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                                <p></p>
                                <div>Definition: </div>
                                <div>${ResultDictionary}</div>
                                <p></p>
                                <div>Example: </div><br>
                                <div>${ResultExample}</div>
                                </div>
                            </div>`

                    }


                    //   for (let example of definitionArray){
                    //     let ResultExample = example.example

                    //     meaningHTML += `
                    //     <div class="card">
                    //       <div class="container">
                    //         <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                    //         <p></p>
                    //         <div>Definition: </div>
                    //         <div>${ResultDictionary}</div>
                    //         <p></p>
                    //         <div>Example: </div><br>
                    //         <div>${ResultExample}</div>
                    //       </div>
                    //     </div>`
                    //     }
                }
            }
            resultCard += `
                <div class="card border-primary mb-3"  style="max-width: 15rem; max-height: 12rem; overflow:auto ;">
                <div class="card-header">${resultWord}</div>
                <div class="card-body text-primary">
                    <h5 class="card-title"></h5>
                    <p class="card-text"></p>
                    ${textHTML +
                audioHTML +
                partOfSpeechHTML +
                meaningHTML}
                    <div class="notebook-icon">
                    <i class="fas fa-trash-alt" onclick=deleteVocab('${result[i].id}')></i>
                </div>
                </div>
                </div>
            `

            // console.log('resultCard: ', resultCard);
            // resultDivSub.innerHTML += resultArray
        // }
    }

    resultDivConSub.innerHTML = resultCard

    // for (let i = 0; i<=result.length; i++){
    //     resultDivSub.innerHTML
    // }

}

// }

loadMyVocab()

async function deleteVocab(id) {
    console.log('delete.....');
    console.log("delete id: ", id);
    await fetch('/deleteMyVocab/' + id, {
        method: 'DELETE'
    })
    loadMyVocab()
}

// function editMemo(id) {
//     Swal.fire({
//       title: 'Edit Memo',
//       input: 'text',
//       inputAttributes: {
//         autocapitalize: 'off',
//       },
//       showCancelButton: true,
//       confirmButtonText: 'Save',
//       showLoaderOnConfirm: true,
//       preConfirm: async text => {
//         let res = await fetch(`/getMyVocab/${id}`, {
//           method: 'PATCH',
//           headers: {
//             'Content-Type': 'application/json',
//           },
//           body: JSON.stringify({ content: text }),
//         })
//         let message = await res.text()
//         if (res.status !== 202) {
//           Swal.showValidationMessage(message)
//         } else {
//           Swal.fire({ title: 'Saved Vocab', text: message })
//           loadMemos()
//         }
//       },
//       allowOutsideClick: () => !Swal.isLoading(),
//     }).then(result => {
//       if (result.isConfirmed) {
//         Swal.fire({
//           title: `${result.value.login}'s avatar`,
//           imageUrl: result.value.avatar_url,
//         })
//       }
//     })
//   }

//   let socket = io.connect()
// socket.on('connect', () => {
//   console.log('connected to socket.io server')
//   socket.on('new-vocab', loadMyVocab)
//   socket.on('deleted-vocab', id => {
//     document.querySelector(`#vocab-${id}`).remove()
//   })
// })