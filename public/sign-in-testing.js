const sign_in_btn = document.querySelector("#sign-in-btn");
const sign_up_btn = document.querySelector("#sign-up-btn");
const container = document.querySelector(".container");

sign_up_btn.addEventListener("click", () => {
    container.classList.add("sign-up-mode");
});

sign_in_btn.addEventListener("click", () => {
    container.classList.remove("sign-up-mode");
});



let signupForm = document.querySelector("#signup-form");
let loginForm = document.querySelector("#login-form");



signupForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  let res = await fetch("/register", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: signupForm.username.value,
      password: signupForm.password.value,
    }),
  });
  console.log("register res:", res);
  if (res.status === 201) {
      alert("sign-up done")
      formReset();
    // $("#signUpPop").modal("hide");
  }
});

//testing
async function toPreviousPage() {
    const urlParams = new URLSearchParams(window.location.search);

    let fromPage = urlParams.get('from-page')
    console.log(fromPage)
    return fromPage
}


loginForm.addEventListener("submit", async (event) => {
  event.preventDefault();

  let res = await fetch("/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    }, // body 只食string
    body: JSON.stringify({
      username: loginForm.username.value,
      password: loginForm.password.value,
    }),
  });
  //await?
  let fromPage = await toPreviousPage()
  console.log("from page:", fromPage)
  console.log("login res:", res);
//   if (res.status === 200) {
//     window.location.href ='/'
//   }
});


function formReset() {
    document.getElementById("login-form").reset();
    document.getElementById("signup-form").reset();
  }




