const sign_in_btn = document.querySelector("#sign-in-btn");
const sign_up_btn = document.querySelector("#sign-up-btn");
const container = document.querySelector(".container");

sign_up_btn.addEventListener("click", () => {
    container.classList.add("sign-up-mode");
});

sign_in_btn.addEventListener("click", () => {
    container.classList.remove("sign-up-mode");
});



let signupForm = document.querySelector("#signup-form");
let loginForm = document.querySelector("#login-form");



signupForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  let res = await fetch("/register", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: signupForm.username.value,
      password: signupForm.password.value,
    }),
  });
  console.log("register res:", res);
  if (res.status === 201) {
      alert("sign-up done")
      formReset();
    // $("#signUpPop").modal("hide");
  }
});





loginForm.addEventListener("submit", async (event) => {
  event.preventDefault();

  let res = await fetch("/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    }, // body 只食string
    body: JSON.stringify({
      username: loginForm.username.value,
      password: loginForm.password.value,
    }),
  });

  console.log("login res:", res);

  // if (res.status === 200) {
  //   window.location.href ='/'
  // }
  let message = await res.text()
  console.log("message=",message)
  if (message === 'Login Success Before Video Selection') {
    window.location.href ='/'
    
  }
  else  {
    let temporary = './video-subtitle.html?vid='+message
    window.location.href = temporary
    // window.location.href ='./exercise.html'
  }
});


function formReset() {
    document.getElementById("login-form").reset();
    document.getElementById("signup-form").reset();
  }




