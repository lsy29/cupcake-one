async function getVideoData() {

    const urlParams = new URLSearchParams(window.location.search);
    console.log("!!!!!!!!", urlParams.get('cat'))
    let catId = urlParams.get('cat')


    const res = await fetch('/video-byCat?cat='+catId)
    const json = await res.json()
    console.log(json)

    const bigBigTitle= document.querySelector('.video-section-title')
    bigBigTitle.innerHTML=json[0].video_genre

    const videoTitle1 = document.querySelector('.title-1')
    videoTitle1.innerHTML=json[0].video_title
    videoTitle1.innerHTML=
    `<a href="./video-subtitle.html?vid=${json[0].id}" class="video-title"> 
    ${json[0].video_title}
    </a>`

    const videoTitle2 = document.querySelector('.title-2')
    videoTitle2.innerHTML=json[1].video_title
    videoTitle2.innerHTML=
    `<a href="./video-subtitle.html?vid=${json[1].id}" class="video-title"> 
    ${json[1].video_title}
    </a>`

    const videoTitle3 = document.querySelector('.title-3')
    videoTitle3.innerHTML=json[2].video_title
    videoTitle3.innerHTML=
    `<a href="./video-subtitle.html?vid=${json[2].id}" class="video-title"> 
    ${json[2].video_title}
    </a>`


    const videoTitle4 = document.querySelector('.title-4')
    videoTitle4.innerHTML=json[3].video_title
    videoTitle4.innerHTML=
    `<a href="./video-subtitle.html?vid=${json[3].id}" class="video-title"> 
    ${json[3].video_title}
    </a>`

    const videoTitle5 = document.querySelector('.video-details.title5')
    videoTitle5.innerHTML=json[4].video_title
    videoTitle5.innerHTML=
    `<a href="./video-subtitle.html?vid=${json[4].id}" class="video-title"> 
    ${json[4].video_title}
    </a>`

    const videoTitle6 = document.querySelector('.title-6')
    videoTitle6.innerHTML=json[5].video_title
    videoTitle6.innerHTML=
    `<a href="./video-subtitle.html?vid=${json[5].id}" class="video-title"> 
    ${json[5].video_title}
    </a>`

    const video1 = document.querySelector('.video-box-1')
    video1.innerHTML =
    `<iframe width="300" height="200" src="https://www.youtube.com/embed/${json[0].youtube_video_id}?controls=0"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>`

    const video2 = document.querySelector('.video-box-2')
    video2.innerHTML =
    `<iframe width="300" height="200" src="https://www.youtube.com/embed/${json[1].youtube_video_id}?controls=0"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>`

    const video3 = document.querySelector('.video-box-3')
    video3.innerHTML =
    `<iframe width="300" height="200" src="https://www.youtube.com/embed/${json[2].youtube_video_id}?controls=0"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>`

    const video4 = document.querySelector('.video-box-4')
    video4.innerHTML =
    `<iframe width="300" height="200" src="https://www.youtube.com/embed/${json[3].youtube_video_id}?controls=0"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>`

    const video5 = document.querySelector('.video-box-5')
    video5.innerHTML =
    `<iframe width="300" height="200" src="https://www.youtube.com/embed/${json[4].youtube_video_id}?controls=0"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>`


    const video6 = document.querySelector('.video-box-6')
    video6.innerHTML =
    `<a href="./video-subtitle.html?id=${json[6].id}" class="video-box-6"><iframe width="300" height="200" src="https://www.youtube.com/embed/${json[5].youtube_video_id}?controls=0"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe></a>`

}

getVideoData()
