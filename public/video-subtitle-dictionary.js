const subtitlePopup = document.querySelector('#dict-popup-subtitle');
const parentElement = document.querySelector('.subtitle-box');
const dictSearchBtnSub = document.querySelector('#dict-search-sub');
let audioID = 0;


parentElement.onmouseover = e => {

    e.target.innerHTML = e.target.innerText.replace(/([\w]+)/g, '<span>$1</span>');
    let children = parentElement.children;
    for (var i = 0; i < children.length; i++) {
        let spanChild = children[i];
        spanChild.onclick = function () {
            subtitlePopup.classList.toggle('show');
            check(spanChild.innerText)
        }
        // Do stuff
    }

};

function check(searchWord) {
    console.log('searching : ', searchWord);
    try{
        document.querySelector('.show').style.display = "initial";
    }catch(error){
        console.log(error);
    }
    parentElement.addEventListener('click', async event => {
        if (!searchWord) {
            return
        }
        const res = await fetch(`/search/${searchWord}`)
        console.log("fetch sucess");
        const result = await res.json()
        const resultDivSub = document.querySelector('#dict-result-subtitle')
        resultDivSub.innerHTML = ""
        // meaningArray = result[0].meanings
        resultWord = /* HTML*/ `<div id="searchWordSub" style=" font-size: 30px"><b>${result.word}</b></div>`
        phoneticsArray = result.phonetics
        console.log('phoneticsArray: ', phoneticsArray);
        meaningArray = result.meanings
        let textHTML = ''
        for (let text of phoneticsArray) {
            let ResultText = text.text
            textHTML += `
          <div>[${ResultText}]</div>
          `
        }
        let audioHTML = ''
        for (let audio of phoneticsArray) {
            audioID++;
            let ResultAudio = audio.audio
            console.log('ResultAudio: ', ResultAudio);
            audioHTML += `
          <button onclick='playMelodia("melodia${audio.text}")'>
            <i class="fas fa-volume-down"></i>
          </button>
          <audio id="melodia${audio.text}" controls="" style="display: none"><source src="${ResultAudio}" type="audio/mp3"></audio>
          `
        }

        console.log('meaningArray: ', meaningArray);
        let meaningHTML = ''
        let partOfSpeechHTML = ''
        for (let meaning of meaningArray) {
            let definitionArray = meaning.definitions
            // let exampleArray = meaning.example
            let partOfSpeech = meaning
            let ResultPartOfSpeech = partOfSpeech.partOfSpeech
            let partOfSpeechSplit = ResultPartOfSpeech.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '')
            let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.')
            // partOfSpeechHTML += `<p font-weight: bold>${ResultPartOfSpeech}</p>`
            for (let definition of definitionArray) {
                console.log('definition = ', definition);
                let ResultDictionary = definition.definition
                // console.log(typeof (result));
                console.log("result:", result);

                let ResultExample = definition.example
                if (ResultExample === undefined) {
                    console.log();
                    ResultExample = `<span style=" color: gray"><i>Not Available</i></span>`
                    meaningHTML += `
                    <div class="card">
                        <div class="container">
                        <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                        <p></p>
                        <div>Definition: </div>
                        <div>${ResultDictionary}</div>
                        <p></p>
                        <div>Example: </div><br>
                        <div>${ResultExample}</div>
                        </div>
                    </div>`
                } else {
                    meaningHTML += `
                    <div class="card">
                        <div class="container">
                        <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                        <p></p>
                        <div>Definition: </div>
                        <div>${ResultDictionary}</div>
                        <p></p>
                        <div>Example: </div><br>
                        <div>${ResultExample}</div>
                        </div>
                    </div>`
                }

                //   for (let example of definitionArray){
                //     let ResultExample = example.example

                //     meaningHTML += `
                //     <div class="card">
                //       <div class="container">
                //         <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                //         <p></p>
                //         <div>Definition: </div>
                //         <div>${ResultDictionary}</div>
                //         <p></p>
                //         <div>Example: </div><br>
                //         <div>${ResultExample}</div>
                //       </div>
                //     </div>`
                //     }
            }
        }
        resultDivSub.innerHTML = resultWord + textHTML + audioHTML + partOfSpeechHTML + meaningHTML
    })
    // const res = await fetch(`/search/${searchWord}`)
    // console.log("fetch sucess");
    // const result = await res.json()
}

dictSearchBtnSub.addEventListener('click', async event =>{
    event.preventDefault()
    dictIcon.classList.add('.dict-hidden');
    dictText.classList.add('.dict-hidden');
    const searchWordSub = document.querySelector("#search-sub").dictCheck.value
    // const searchWord = 'test'
    console.log("earchWord: ", searchWordSub);
      if (!searchWordSub){
        return 
      }
      const res = await fetch(`/search/${searchWordSub}`)
      console.log("fetch sucess");
      const result = await res.json()
      const resultDivSub = document.querySelector('#dict-result-subtitle')
      resultDivSub.innerHTML = ""
      // meaningArray = result[0].meanings
      resultWord = `<div style=" font-size: 30px"><b>${result.word}</b></div>`
      phoneticsArray = result.phonetics
      console.log('result: ', result);
      meaningArray = result.meanings
  
      let textHTML = ''
      for (let text of phoneticsArray){
        let ResultText = text.text
        textHTML += `
        <div>[${ResultText}]</div>
        `
      }
      let audioHTML = ''
      for (let audio of phoneticsArray){
        let ResultAudio = audio.audio
        console.log('ResultAudio: ', ResultAudio);
        audioHTML += `
        <button onclick='playMelodia("melodia${audio.text}")'>
          <i class="fas fa-volume-down"></i>
        </button>
        <audio id="melodia${audio.text}" controls="" style="display: none"><source src="${ResultAudio}" type="audio/mp3"></audio>
        `
      }
  
      console.log('meaningArray: ', meaningArray);
      let meaningHTML = ''
      let partOfSpeechHTML = ''
      let ResultDictionary = ''
      for (let meaning of meaningArray){
          let definitionArray = meaning.definitions
          // let exampleArray = meaning.example
          let partOfSpeech = meaning
          let ResultPartOfSpeech = partOfSpeech.partOfSpeech
          // if(ResultPartOfSpeech == 'transitive verb'){
          //   ResultPartOfSpeech = 'v.t.';
          // }else if(ResultPartOfSpeech == 'transitive verb'){
  
          // }
          let partOfSpeechSplit = ResultPartOfSpeech.split(/\s/).reduce((response,word)=> response+=word.slice(0,1),'')
          let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.')
          // partOfSpeechHTML += `<p font-weight: bold>${ResultPartOfSpeech}</p>`
          for (let definition of definitionArray){
              console.log('definition = ', definition);
              ResultDictionary = definition.definition
              console.log(typeof(result));
  
              let ResultExample = definition.example
              if (ResultExample === undefined){
                  console.log();
                  ResultExample = `<span style=" color: gray"><i>Not Available</i></span>`
                  meaningHTML += `
                  <div class="card">
                      <div class="container">
                      <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                      <p></p>
                      <div>Definition: </div>
                      <div>${ResultDictionary}</div>
                      <p></p>
                      <div>Example: </div><br>
                      <div>${ResultExample}</div>
                      </div>
                  </div>`
              }else {
                  meaningHTML += `
                  <div class="card">
                      <div class="container">
                      <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                      <p></p>
                      <div>Definition: </div>
                      <div>${ResultDictionary}</div>
                      <p></p>
                      <div>Example: </div><br>
                      <div>${ResultExample}</div>
                      </div>
                  </div>`
              }
          }
  
  
      }
      // meaningHTML += '</ul>'
      resultDivSub.innerHTML = resultWord + textHTML + audioHTML + partOfSpeechHTML + meaningHTML
  })

async function addMyVocab(myVocabWord) {
    myVocabWord = document.querySelector('#searchWordSub').innerText;
    console.log("myVocabWord: ", myVocabWord);
    let res = await fetch('/myVocab', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            myVocabWord: myVocabWord,
        }),
    })
    console.log('fetch successfully');
    console.log('res.body: ', await res.json());
    
}

document.querySelector('.close').addEventListener("click", function() {
	document.querySelector('.show').style.display = "none";
});