async function getSubtitle() {
    const urlParams = new URLSearchParams(window.location.search);
    console.log(urlParams.get('vid'))
    let videoId = urlParams.get('vid')
    // const res = await fetch('/subtitle?videoId=blf-JfKmr2c')
    const res = await fetch('/subtitle?vid='+videoId)
    // const res = await fetch('/subtitle')
    const json = await res.json()
    console.log(json)

    const subtitle = document.querySelector('.subtitle-box')
    subtitle.innerHTML = json[0].subtitle

    const bigTitle = document.querySelector('.video-section-title')
    bigTitle.innerHTML = json[0].video_title

    const video = document.querySelector('.video-box')
    // wtf is ?enablejsapi=1 
    video.innerHTML +=
    `<p><iframe id="${json[0].youtube_video_id}" src="https://www.youtube.com/embed/${json[0].youtube_video_id}?enablejsapi=1&controls=1" width="450" height="350"
    allowfullscreen="allowfullscreen"></iframe></p>`
    // for (const videoTitle of json) {
    // videoTitle.innerHTML += `<a href="#">${store.name}</a>`
    //     }
}

getSubtitle()


parentElement.onmouseover = e => {
    console.log('hover effect');

    e.target.innerHTML = e.target.innerText.replace(/([\w]+)/g, '<span>$1</span>');
    let children = parentElement.children;
    for (var i = 0; i < children.length; i++) {
        let spanChild = children[i];
        spanChild.onclick = function () {
            subtitlePopup.classList.toggle('show');
            check(spanChild.innerText)
        }
        // Do stuff
    }

};

function check(searchWord) {
    console.log('searching : ', searchWord);
    document.querySelector('.show').style.display = "initial";
    parentElement.addEventListener('click', async event => {
        if (!searchWord) {
            return
        }
        const res = await fetch(`/search/${searchWord}`)
        console.log("fetch sucess");
        const result = await res.json()
        const resultDivSub = document.querySelector('#dict-result-subtitle')
        resultDivSub.innerHTML = ""
        // meaningArray = result[0].meanings
        resultWord = /* HTML*/ `<div id="searchWordSub" style=" font-size: 30px"><b>${result.word}</b></div>`
        phoneticsArray = result.phonetics
        console.log('phoneticsArray: ', phoneticsArray);
        meaningArray = result.meanings
        let textHTML = ''
        for (let text of phoneticsArray) {
            let ResultText = text.text
            textHTML += `
          <div>[${ResultText}]</div>
          `
        }
        let audioHTML = ''
        for (let audio of phoneticsArray) {
            let ResultAudio = audio.audio
            console.log('ResultAudio: ', ResultAudio);
            audioHTML += `
          <button onclick='playMelodia("melodia${audio.text}")'>
            <i class="fas fa-volume-down"></i>
          </button>
          <audio id="melodia${audio.text}" controls="" style="display: none"><source src="${ResultAudio}" type="audio/mp3"></audio>
          `
        }

        console.log('meaningArray: ', meaningArray);
        let meaningHTML = ''
        let partOfSpeechHTML = ''
        for (let meaning of meaningArray) {
            let definitionArray = meaning.definitions
            // let exampleArray = meaning.example
            let partOfSpeech = meaning
            let ResultPartOfSpeech = partOfSpeech.partOfSpeech
            let partOfSpeechSplit = ResultPartOfSpeech.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '')
            let partOfSpeechUI = partOfSpeechSplit.split("").reverse().join('.')
            // partOfSpeechHTML += `<p font-weight: bold>${ResultPartOfSpeech}</p>`
            for (let definition of definitionArray) {
                console.log('definition = ', definition);
                let ResultDictionary = definition.definition
                console.log(typeof (result));

                let ResultExample = definition.example
                if (ResultExample === undefined) {
                    console.log();
                    ResultExample = `<span style=" color: gray"><i>Not Available</i></span>`
                    meaningHTML += `
                    <div class="card">
                        <div class="container">
                        <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                        <p></p>
                        <div>Definition: </div>
                        <div>${ResultDictionary}</div>
                        <p></p>
                        <div>Example: </div><br>
                        <div>${ResultExample}</div>
                        </div>
                    </div>`
                } else {
                    meaningHTML += `
                    <div class="card">
                        <div class="container">
                        <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                        <p></p>
                        <div>Definition: </div>
                        <div>${ResultDictionary}</div>
                        <p></p>
                        <div>Example: </div><br>
                        <div>${ResultExample}</div>
                        </div>
                    </div>`
                }

                //   for (let example of definitionArray){
                //     let ResultExample = example.example

                //     meaningHTML += `
                //     <div class="card">
                //       <div class="container">
                //         <div class=partOfSpeechUI><b>${partOfSpeechUI}.</b></div>
                //         <p></p>
                //         <div>Definition: </div>
                //         <div>${ResultDictionary}</div>
                //         <p></p>
                //         <div>Example: </div><br>
                //         <div>${ResultExample}</div>
                //       </div>
                //     </div>`
                //     }
            }
        }
        resultDivSub.innerHTML = resultWord + textHTML + audioHTML + partOfSpeechHTML + meaningHTML
    })
    // const res = await fetch(`/search/${searchWord}`)
    // console.log("fetch sucess");
    // const result = await res.json()
}

async function addMyVocab(myVocabWord) {
    myVocabWord = document.querySelector('#searchWordSub').innerText;
    console.log("myVocabWord: ", myVocabWord);
    let res = await fetch('/myVocab', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            myVocabWord: myVocabWord.value,
        }),
    })
    console.log('fetch successfully');
    console.log('res.body: ', await res.json());
}



