import express from 'express'
import SubtitleController from './controllers/subtitle-controller'
import VideoController  from './controllers/video-controller'

// import { Multer } from 'multer'
import { UserController } from './controllers/user-controller'
import { DictionaryController } from './controllers/dictionary-controller'

import { ExerciseController } from './controllers/exercise-controller'
import { MyVocabController } from './controllers/myVocab-controller'

// use the isLoggedIn guard to make sure only admin can call this API
export function createRouter(options: {
  subtitleController: SubtitleController
  videoController: VideoController

  // upload: Multer
  isLoggedIn: express.RequestHandler
  userController: UserController
  dictionaryController: DictionaryController
  myVocabController: MyVocabController
  exerciseController: ExerciseController
}) {
  // const { subtitleController, isLoggedIn, userController} = options// cupcake graffiti
  const { isLoggedIn, userController, dictionaryController, myVocabController, subtitleController, exerciseController, videoController} = options// cupcake graffiti
  // const { memoController, isLoggedIn, userController, dictionaryController,exerciseController} = options// cupcake graffiti

  let router = express.Router()


  // user routes
  router.get('/login/google', userController.loginWithGoogle)
  router.post('/register', userController.register)
  router.post('/login', userController.login)
  router.post('/logout', userController.logout)
  router.get('/role', userController.getRole)
  router.get('/getUserProfile', userController.getUserProfile)
  

  //dictonary routes
  router.get('/search/:searchWord', dictionaryController.searchWord)

  // my vocab routes
  router.post('/myVocab', isLoggedIn, myVocabController.getWordFromDB)
  router.get('/getMyVocab', isLoggedIn, myVocabController.getAllVocabs)
  router.delete('/deleteMyVocab/:id', isLoggedIn, myVocabController.deleteVocab)
  // router.post('/myVocab', isLoggedIn, myVocabController.getWordFromDB)

  //exercise routes
  router.get('/GETEXERCISE', exerciseController.htmlGetEx)

  

  router.post('/postanwser', exerciseController.htmlPostAns)
  // router.get('/getanswer', exerciseController.htmlGetAns)
  // pls refer to the fetch in video-bbc.js
  router.get('/video-byCat', videoController.getTitleAndNumber)
  router.get('/getmodelanswer', exerciseController.htmlGetModelAns)
  router.get('/getyouranswer', exerciseController.htmlGetYourAns)
  router.get('/getexinfo', exerciseController.htmlGetInfo)

  router.get('/subtitle', subtitleController.getSubtile)
  router.get('/dummy',isLoggedIn, subtitleController.getSubtile)

  //only logged in users can enter exercise.html
  router.get('/exercise.html', isLoggedIn)


  
  return router
}
