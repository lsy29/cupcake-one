import { Knex } from 'knex'
import { hashPassword } from '../hash'

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex('users').del()

  // Inserts seed entries
  await knex('users').insert([
    {
      username: 'alice',
      password: await hashPassword('wong'),
      email: 'alice@gmail.com',
      google_access_token: 'ya29.a0AfH6SMBLdXURob6hkV_mJPuKXJ7zeP0TpF7bv-V1-0QGPFyaTNavqLJUnpdZlAWlAWX1cvli6jiQQKOgV3bwax-imldv3dLBtmkBXkTz2cUrnI5wP0w_vhub6CGtz_49-bzg3sgUhEreikFv4z14QjQiAt8N',
      role: 'admin',
      level: '8',
      score: '200',
      coins: '500'
    }
  ]).returning('id');
}
