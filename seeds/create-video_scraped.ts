import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("video_scraped").del();

    // Inserts seed entries
    await knex("video_scraped").insert([
        {
            video_title: 'Alu bukaharay ki Chatni By Home Infotainer Cooking with Passion',
            youtube_video_id: 'h9hi0uLPQzs',
            video_url: 'https://youtu.be/h9hi0uLPQzs',
            length: '199'
        },
        {
            video_title: ' Die große Milka Kochschlacht',
            youtube_video_id: 'vztpQw8NUJY',
            video_url: 'https://youtu.be/vztpQw8NUJY',
            length: '112'
        },
        {
            video_title: 'Cooking x destroyer for a minute straight',
            youtube_video_id: 'w4awfCN3OUQ',
            video_url: 'https://youtu.be/w4awfCN3OUQ',
            length: '103'
        },
        {
            video_title: 'How to Revive Wilted Vegetables For Cooking',
            youtube_video_id: '5tHnQvtGX-A',
            video_url: 'https://youtu.be/5tHnQvtGX-A',
            length: '186'
        },
        {
            video_title: 'Cooking With Eren |Gacha Club|',
            youtube_video_id: 'iCJmEXgef8Y',
            video_url: 'https://youtu.be/iCJmEXgef8Y',
            length: '149'
        }
    ]).returning('id');
};
