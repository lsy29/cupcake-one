// import { Dictionary } from '../models/dictionary'
import { Knex } from 'knex'
import fetch from 'node-fetch'


export class DictionaryService {
    constructor(private knex: Knex) {}

    // async searchWord(searchWord: string){
    //     // let checkExist = await this.knex.select('word').from('dictionary');
    //     return await this.knex.select('id', 'user_id', 'word', 'phonetics', 'meanings').from('dictionary').where({word: searchWord}).first()
    // }

    async getWordFromDB(searchWord: string) {
        return await this.knex.select('id', 'word', 'phonetics', 'meanings')
            .from('dictionary')
            .where({word: searchWord}).first()
    }

    async searchFromAPI(searchWord: string) {

        // let result = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en_US/${searchWord}`)
        let result = await fetch(`https://api.dictionaryapi.dev/api/v2/entries/en_US/${searchWord}`)
        let dataArr = await result.json()
        let phonetics = [] as any
        phonetics = dataArr[0].phonetics

        let meanings = [] as any
        
        for(let data of dataArr){
            let innerMeanings = data.meanings
            for(let meaning of innerMeanings){
                meanings.push (meaning)
            }
            
        }
        console.log('=========================================================');
        
        console.log("searchWord = ", searchWord);
        console.log("phonetics = ", phonetics);
        console.log("meanings = ", meanings);

        
        let insertResult = await this.knex
            .insert({
                word: searchWord, 
                phonetics: JSON.stringify(phonetics),
                meanings: JSON.stringify(meanings)
                
                // antonym: data.antonym,
            })
            .into('dictionary')
            .returning('*')
  
            // console.log('insertResult: ', insertResult);
            // console.log('insertResult[0]: ', insertResult[0]);
        
        return insertResult[0]
    }

}