// import { Memo } from '../models/memo'
// import fs from 'fs'
// import path from 'path'
import { Knex } from 'knex'

export class ExerciseService {
  constructor(private knex: Knex) {}

  

  async getExSql(exId: number) {

    return this.knex
    .select('content')
    .from('exercises')
    .where({id:exId})
  }

  async getInfoSql(vdoID:number,userId:number) {

    let username = await this.knex
    .select('username')
    .from('users')
    .where({id:userId})

    let ytid = await this.knex
    .select('youtube_video_id')
    .from('video_scraped')
    .where({id:vdoID})

    let vdourl = await this.knex
    .select('video_url')
    .from('video_scraped')
    .where({id:vdoID})

    return [username,ytid,vdourl]

  }

  async getModelAnsSql(ansId: number) {
    // console.log("from service, ansId=", ansId);
    return this.knex
    .select('ex_id','model_answer1','model_answer2','model_answer3','model_answer4','model_answer5')
    .from('exercises_answer_generated')
    .where({id:ansId})
  }

  async getYourAnsSql(userId: number,ansId: number) {
    // console.log("from service, ansId=", ansId);
    return this.knex
    .select('exercises_id',' user_id ','created_at','attempt','highest_score','posted_answer1','posted_answer2','posted_answer3','posted_answer4','posted_answer5')
    .from('my_exercises')
    .where({exercises_id:ansId})
    .andWhere({user_id:userId})
    .orderBy('created_at', 'desc')
  }

  async postAnsSql(session_user_id:number , session_ex_id:number , answer1:string , answer2:string , answer3:string  , answer4:string , answer5:string ) {
    
    let anwserRef= await this.knex
    .select('ex_id','model_answer1','model_answer2','model_answer3','model_answer4','model_answer5')
    .from('exercises_answer_generated')
    .where({id:session_ex_id})

    // console.log('anwserRef : ',anwserRef)

    let points = 0 ; // initialisation

    if (answer1 == anwserRef[0].model_answer1) {points = points +1}
    if (answer2 == anwserRef[0].model_answer2) {points = points +1}
    if (answer3 == anwserRef[0].model_answer3) {points = points +1}
    if (answer4 == anwserRef[0].model_answer4) {points = points +1}
    if (answer5 == anwserRef[0].model_answer5) {points = points +1}

    let highestScore = await this.knex
    .select('attempt')
    .from('my_exercises')
    .where({exercises_id:session_ex_id})
    .andWhere({user_id:session_user_id})
    .orderBy('attempt', 'desc')

    console.log('highestScore ::: ',highestScore)

    let highestRecord :number =0    

    if ( highestScore.length == 0 ) { highestRecord = points}
    else {
          highestRecord = highestScore[0].attempt
          if (points > highestRecord ) { highestRecord = points}
    }
    



    return this.knex
    .insert({
      user_id: session_user_id,
      exercises_id : session_ex_id,
      attempt : points , // attempt is now storing POINTs for that exercise ; if heor she has two records for Point at that exercise , that means attempt =2
      highest_score : highestRecord ,
      posted_answer1 : answer1,
      posted_answer2 :answer2,
      posted_answer3 : answer3,
      posted_answer4 :answer4,
      posted_answer5 :answer5

    })
    .into('my_exercises')
    .returning('id')
  }


  

  
  }

