import { Memo } from '../models/memo'
import fs from 'fs'
import path from 'path'
import { Knex } from 'knex'

export class MemoService {
  constructor(private knex: Knex) {}

  async createMemo(memo: { content: string; filename: string | null }) {
    let [id] = await this.knex
      .insert({
        content: memo.content,
        image: memo.filename,
      })
      .into('memos')
      .returning('id')
    return id
  }

  async getAllMemos(): Promise<Memo[]> {
    return this.knex
      .select('id', 'content', 'image')
      .from('memos')
      .orderBy('updated_at', 'desc')
  }

  async updateMemo(memo: { id: number; content: string }): Promise<number> {
    return this.knex('memos')
      .update({
        content: memo.content,
        updated_at: this.knex.fn.now(),
      })
      .where({ id: memo.id })
  }

  async deleteMemo(id: number) {
    const row = await this.knex('memos').select('image').where({ id }).first()
    let filename = row?.image
    if (filename) {
      fs.unlink(path.join('uploads', filename), err => {
        if (err) {
          console.log('failed to delete memo image:', filename, 'error:', err)
        }
      })
    }
    await this.knex('memos').where({ id }).del()
  }
}
