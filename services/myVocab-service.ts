// // import fs from 'fs'
// // import path from 'path'
import { Knex } from 'knex'
import { MyVocab } from '../models/myVocab'

export class MyVocabService {
    constructor(private knex: Knex) {}

    async postResultToDB(myVocabWord: string, user_id: number) {
        // console.log('user_id: ', user);
        let dict_id = await this.knex
            .select("id")
            .from('dictionary')
            // .join('my_vocab', 'my_vocab.dictionary_id', '=', 'dictionary.id')
            .where({word: myVocabWord}).first()
    console.log('user_id=', user_id);
    console.log('dict_id = ', dict_id);


        let insertToMyVocabDB = await this.knex
            .insert({
                user_id: user_id,
                dictionary_id: dict_id.id
            })
            .into('my_vocab')
            .returning('*')

        return insertToMyVocabDB[0]
    }

    // async receiveWord(myVocabWord: string) {
    //     return this.knex
    //         .select('*')
    //         .from('dictionary')
    //         .join('my_vocab', 'my_vocab.dictionary_id', '=', 'dictionary.id')
    //         .where({word: myVocabWord})
    // }

    async getAllVocabs(user_id: number): Promise<MyVocab[]> {
        return this.knex
            .select('*')
            .from('dictionary')
            .join('my_vocab', 'my_vocab.dictionary_id', '=', 'dictionary.id')
            .where({user_id: user_id})
    }

    async deleteVocab(id: number){
        await this.knex('my_vocab')
            .where({id})
            .del()
        
    }
    

    // async getMyVocabFromDB(myVocabWord: string) {
    //     return await this.knex
    //         .select("*")
    //         .from('dictionary')
    //         .join('my_vocab', 'my_vocab.dictionary_id', '=', 'dictionary.id')
    //         .where({word: myVocabWord})
    // }
        

    // }
}