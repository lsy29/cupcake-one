//get youtube videos' id, title and subtitle from database

import { Knex } from 'knex'
class SubtitleService {
    constructor(private knex: Knex) { }

    public async getSubtitle(videoId: number) {
        // qb.select('*').from('people').join('ancestors', 'ancestors.parentId', 'people.id')
        return await this.knex
        // .select('*')
        .select("video_scraped.youtube_video_id","video_scraped.video_title","subtitle.subtitle")
        .from("subtitle")
        .join("video_scraped","subtitle.video_id","video_scraped.id")
        .where({'video_scraped.id':videoId})

//         select vs.video_title, vs.youtube_video_id,s.video_id, s.subtitle 
//          from video_scraped as vs join subtitle as s 
//          on vs.id = s.video_id 
//          where youtube_video_id='nS179aVtt4U';
    }
  
}

export = SubtitleService
