import fetch from 'node-fetch'
import { GoogleResult, User } from '../models/user'
import { hashPassword } from '../hash'
import { Knex } from 'knex'

export class UserService {
  constructor(private knex: Knex) {}

  async createUser(username: string, password: string, role:string) {
    let passwordHash = await hashPassword(password)
    return this.knex
      .insert({ username, password: passwordHash, role })
      .into('users')
      .returning('id')
  }

  async getUserByUserName(username: string): Promise<User> {
    return this.knex.select('*').from('users').where({ username }).first()
  }

  async createOrUpdateGoogleUser(accessToken: string): Promise<User> {
    const fetchRes = await fetch(
      'https://www.googleapis.com/oauth2/v2/userinfo',
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      },
    )
    const fetchResult: GoogleResult = await fetchRes.json()
    console.log ("fetchResult:",fetchResult)
    

    let user: User = await this.knex
      .select('*')
      .from('users')
      .where({ email: fetchResult.email })
      .first()

      console.log('select user result = ', user);
      
    // if this user is new comer
    if (!user) {
    
      let  users = await this.knex
        .insert({
          username: fetchResult.name,
          user_icon: fetchResult.picture,
          email: fetchResult.email,
          google_access_token: accessToken,
          
        })
        .into('users')
        .returning('*')
        
      console.log('before delete user = ', users);
      user = users[0]
      delete user.google_access_token
      return user
    }

    // if this user already registered

    await this.knex('users')
      .update({ google_access_token: accessToken })
      .where({ id: user.id })

    user.google_access_token = accessToken
    delete user.password
    return user
  }
}
