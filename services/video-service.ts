
import { Knex } from 'knex'

class VideoService {
    constructor(private knex: Knex) { }

    //testing
    public async getTitleAndNumber (category: string){
        return await this.knex
        // .select("video_scraped.video_genre","subtitle.video_id","video_scraped.youtube_video_id","video_scraped.video_title")
        // .from("subtitle")
        // .join("video_scraped","subtitle.video_id","video_scraped.id")
        // // .where('video_genre=BBC')
        // .where({'video_scraped.id':videoId})
        .select("*")
        .from("video_scraped")
        .where({"video_genre":category})
    }
}

export = VideoService
