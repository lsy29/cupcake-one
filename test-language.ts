const LanguageDetect = require('languagedetect');
const lngDetector = new LanguageDetect();
 
// OR
// const lngDetector = new (require('languagedetect'));
let detectInputs =[
    'Ek Dafa - Ujjwal Vats |☮ official audio | latest hindi songs 2021',
    'Asian lady sings a very hot English song with Rap: Bang Bang-Ariana Grande Cover by DansySingsBig',
    "Bang Bang 是小天后Ariana Grande的一首非常劲爆的英文热歌。 She got a body like an hourglass, But I can give it to you all the timeShe got a booty like a Cadillac, But I can send you into overdrive (Oh)You've been waiting for that, Step up, swing your batSee anybody could be bad to you You need a good girl to blow your mind, yeahBang bang into the room (I know you want it) Bang bang all over you (I'll let you have it)Wait a minute, let me take you there (Ah) Wait a minute 'til ya (Ah, hey!)Bang bang there goes your heart (I know you want it) Back, back seat of my car (I'll let you have it)Wait a minute, let me take you there (Ah) Wait a minute 'til ya (Ah, hey!)She might'a let you hold her hand in school But I'ma show you how to graduateNo, I don't need to hear you talk the talk Just come and show me what your momma gave ya (Oh, yeah)I heard you've got a very big (Shh) Mouth but don't say a thingSee anybody could be good to you You need a bad girl to blow your mindBang bang into the room (I know you want it) Bang bang all over you (I'll let you have it)Wait a minute, let me take you there (Ah) Wait a minute 'til ya (Ah, hey!)Bang bang there goes your heart (I know you want it) Back, back seat of my car (I'll let you have it)Wait a minute, let me take you there (Ah) Wait a minute 'til ya (Ah, yeah)It's Myx Moscato, It's frizz in a bottle It's Dansy full throttle, It's oh, ohSwimming in the grotto, We winning in the lotto, We dipping in the pot of blue foam, soKitten so good, It's dripping on wood Get a ride in the engine that could goBatman robbin' it, Bang, bang, cockin' it Queen Nicki dominant, prominentIt's me, Dansy, and DansySingsBig If they test me they sorryRide his uh, like a Harley, Then pull off in his FerrariIf he hangin' we bangin', Phone rangin', he slangin'It ain't karaoke night but get the mic 'cause I'm singin' (Uh)B to the A, to the N, to the G, to the uh (Baby) B to the A, to the N, to the G, to the, heySee anybody could be good to you You need a bad girl to blow your mind (Your mind)"
]
for (let detectInput of detectInputs){
    console.log(detectInput.substring(0,20),':',lngDetector.detect(detectInput,3));
}

// console.log(lngDetector.getLanguages(detectInput));

 
/*
  [ [ 'english', 0.5969230769230769 ],
  [ 'hungarian', 0.407948717948718 ],
  [ 'latin', 0.39205128205128204 ],
  [ 'french', 0.367948717948718 ],
  [ 'portuguese', 0.3669230769230769 ],
  [ 'estonian', 0.3507692307692307 ],
  [ 'latvian', 0.2615384615384615 ],
  [ 'spanish', 0.2597435897435898 ],
  [ 'slovak', 0.25051282051282053 ],
  [ 'dutch', 0.2482051282051282 ],
  [ 'lithuanian', 0.2466666666666667 ],
  ... ]
*/
 
// Only get the first 2 results
// console.log(lngDetector.detect('This is a test.', 2));
 
/*
  [ [ 'english', 0.5969230769230769 ], [ 'hungarian', 0.407948717948718 ] ]
*/