import { Browser, firefox, Page } from 'playwright'
import { knex } from '../db'
import { env } from '../env'
import { main } from '../main'

let mode = 'dev'
mode = 'auto'

describe('End-to-End TestSuit of create memo', () => {
  let browser: Browser
  let page: Page
  let server: { close: () => Promise<void> }
  beforeAll(async () => {
    server = await main(env.PORT)
    browser = await firefox.launch({ headless: mode === 'auto' })
    page = await browser.newPage()
  })
  afterAll(async () => {
    if (mode === 'dev') {
      return
    }
    await page.close()
    await browser.close()
    await server.close()
    setTimeout(() => {
      process.exit()
    }, 5000)
  })
  beforeEach(async () => {
    await knex('memos').del()
    await knex('memos').insert([
      { content: 'post 1', image: '1.jpg' },
      { content: 'post 2', image: null },
    ])
  })

  it('should show home page with "Memo Wall" on document title', async () => {
    await page.goto(`http://localhost:${env.PORT}`)

    // let title = await page.evaluate(() => {
    //   return document.title
    // })

    let title = await page.title()
    expect(title).toContain('Memo Wall')
  })

  it('should show sample memos on the page', async () => {
    // console.log = jest.fn()
    await page.goto(`http://localhost:${env.PORT}`)
    await page.waitForFunction(
      () => document.querySelectorAll('.memo').length > 0,
    )
    let memos = await page.evaluate(() => {
      let memos: string[] = []
      document.querySelectorAll('.memo').forEach(memo => {
        memos.push(memo.textContent!)
      })
      return memos
    })
    expect(memos).toHaveLength(2)
    expect(memos[0]).toContain('post 1')
    expect(memos[1]).toContain('post 2')
  })
})
