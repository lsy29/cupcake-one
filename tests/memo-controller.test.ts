import { Server } from 'socket.io'
import { MemoController } from '../controllers/memo-controller'
import { MemoService } from '../services/memo-service'
import express from 'express'

describe('MemoController', () => {
  let memoController: MemoController
  let memoService: MemoService
  let io: Server
  let req: express.Request
  let res: express.Response
  let resEnd: jest.Mock

  beforeEach(() => {
    io = {} as any
    io.emit = jest.fn()

    memoService = {} as any
    memoService.createMemo = jest.fn()

    memoController = new MemoController(memoService, io)

    req = {} as any
    req.body = {}

    res = {} as any
    resEnd = jest.fn()
    res.status = jest.fn(() => {
      return {
        end: resEnd,
      } as any
    })
  })

  describe('createMemo', () => {
    // happy path with content
    it('should create memo with content', async () => {
      req.body.content = 'hello world'
      await memoController.createMemo(req, res)
      expect(memoService.createMemo).toBeCalled()
      let calledWith = (memoService.createMemo as jest.Mock).mock.calls[0][0]
      expect(calledWith).toBeDefined()
      expect(calledWith.content).toBe('hello world')
      expect(res.status).toBeCalledWith(201)
      expect(resEnd).toBeCalledWith('created')
      expect(io.emit).toBeCalledWith('new-memo')
    })

    // happy path with image
    it('should create memo with image', async () => {
      req.file = {
        filename: 'image.jpg',
      } as any
      await memoController.createMemo(req, res)
      expect(memoService.createMemo).toBeCalled()
      let calledWith = (memoService.createMemo as jest.Mock).mock.calls[0][0]
      expect(calledWith).toBeDefined()
      expect(calledWith.filename).toBe('image.jpg')
      expect(res.status).toBeCalledWith(201)
      expect(resEnd).toBeCalledWith('created')
      expect(io.emit).toBeCalledWith('new-memo')
    })

    // expected invalid input
    it('should not create memo without content nor image', async () => {
      await memoController.createMemo(req, res)
      expect(memoService.createMemo).not.toBeCalled()
      expect(res.status).toBeCalledWith(400)
      expect(resEnd).toBeCalledWith(
        'require content or image but both are not given',
      )
      expect(io.emit).not.toBeCalled()
    })
  })
})
