import { MemoService } from '../services/memo-service'
import Knex from 'knex'

let configs = require('../knexfile')

describe('memo-service.ts TestSuit', () => {
  let memoService: MemoService
  let knex = Knex(configs.test)
  beforeAll(async () => {
    await knex.migrate.rollback({}, true)
    await knex.migrate.latest()
  })
  beforeEach(async () => {
    memoService = new MemoService(knex)
  })
  afterAll(async () => {
    await knex.destroy()
  })

  it('should load knex', async () => {
    let result = await knex.raw(`select * from user;`)
    expect(result.rows).toHaveLength(1)
    // console.log(memoService);
  })

  it('should create memo', async () => {
    let id = await memoService.createMemo({
      content: 'Hello World',
      filename: null,
    })
    expect(id).toBeGreaterThanOrEqual(1)
    let rows = await knex('memos').select('content', 'image').where({ id })
    expect(rows).toHaveLength(1)
    expect(rows[0].content).toBe('Hello World')
    expect(rows[0].image).toBeNull()
  })

  it('should get all memos', async () => {
    await knex('memos').del()
    await knex('memos').insert([
      { content: 'post 1', image: '1.jpg' },
      { content: 'post 2', image: '2.jpg' },
      { content: 'post 3', image: null },
    ])
    const memos = await memoService.getAllMemos()
    expect(memos).toHaveLength(3)

    expect(memos[0]).toBeTruthy()
    expect(memos[0].content).toBe('post 1')
    expect(memos[0].image).toBe('1.jpg')

    expect(memos[1]).toBeTruthy()
    expect(memos[1].content).toBe('post 2')
    expect(memos[1].image).toBe('2.jpg')

    expect(memos[2]).toBeTruthy()
    expect(memos[2].content).toBe('post 3')
    expect(memos[2].image).toBeNull()
  })

  it('should update memo', async () => {
    let [id] = await knex('memos')
      .insert({ content: 'old version' })
      .returning('id')
    await memoService.updateMemo({ id, content: 'new version' })
    let memo = await knex('memos').select('content').where({ id }).first()
    expect(memo).toBeTruthy()
    expect(memo.content).toBe('new version')
  })

  it('should delete memo', async () => {
    let [id] = await knex('memos')
      .insert({ content: 'the memo to be deleted' })
      .returning('id')
    await memoService.deleteMemo(id)
    let row = await knex('memos').count().where({ id }).first()
    expect(row).toEqual({ count: '0' })
  })
})
