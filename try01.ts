//

import { knex } from './db'



//auto generation for exercise and model answer from dbLenBefore to dbLenAfter
async function exercise_generator() {
    let modelAns = ['','','','','']
    let a = await knex.select('id','video_id','subtitle').from('subtitle').where({id:6}) ; // where({id:8})(one of the stt_id) supposedly sent from frontend
        
    let stt_id =a[0].subtitle.id
    let vdo_id =a[0].subtitle.video_id

        console.log(typeof(a[0].subtitle))
        let sttOriginal=a[0].subtitle.replace(/\n/g, " ").replace(/,/g, ', ')//.replace(/\./g, '. ')
        console.log(typeof(sttOriginal))
        console.log("")
        console.log("")

        console.log(sttOriginal)

        console.log("")
        console.log("")
        console.log("")

        let sttArray=sttOriginal.split(" ")
        let exArray=Array.from(sttArray)

        for (let j=0; j<5 ; j++){
            let randomWord = Math.floor(Math.floor(sttArray.length/5)*Math.random())+j*Math.floor(sttArray.length/5);
            let qNum=j+1
            exArray[randomWord] = " [   "+qNum+"   ] ";
            modelAns[j]=sttArray[randomWord]
    }   
    
    
    let questionPrint=exArray.join(' ');
    console.log (questionPrint)
    console.log("")
    console.log("")
    console.log("")
    console.log (modelAns)

    let questionJSON = JSON.stringify(questionPrint)

    let exID = await knex.insert({
        video_id:vdo_id ,
        subtitle:stt_id , // may spelling mistake 'subtitle' should be stt_id
        content:questionJSON,
        created_at:knex.fn.now(),
        updated_at:knex.fn.now(),
    })
    .into('exercises')
    .returning('id');

    let answer_id = await knex.insert({
        ex_id:exID[0] ,
        model_answer1:modelAns[0] , 
        model_answer2:modelAns[1] , 
        model_answer3:modelAns[2] , 
        model_answer4:modelAns[3] , 
        model_answer5:modelAns[4] , 
        // created_at:knex.fn.now(),
        // updated_at:knex.fn.now(),
    })
    .into('exercises_answer_generated')
    .returning('id');


    console.log('exercise_id',exID)

    console.log('answer_set_id',answer_id)
    knex.destroy()
}

exercise_generator() // run only if the requested ex dosn't exist  for that vdo (i.e. stt) 




