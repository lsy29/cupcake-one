// ES5
var getSubtitles = require('youtube-captions-scraper').getSubtitles;
 
getSubtitles({
  videoID: 'WiO7I4UhosE', // youtube video id
  lang: 'en' // default: `en`
}).then(function(captions:any) {
  console.log(captions);
});