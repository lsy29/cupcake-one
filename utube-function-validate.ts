// function 1: get transcript by video id
// insert video id & transcript into db

// for old version to import
var getSubtitles = require('youtube-captions-scraper').getSubtitles;
import youtube from 'scrape-youtube';
import { knex } from './db'
import { videoCat } from './videoCat';
const LanguageDetect = require('languagedetect');
const lngDetector = new LanguageDetect();
// videoCat:string[] = ["BBC", "Netflix", "Disney", "Health", "Songs", "Science"]
let dbLenBefore
let dbLenAfter
let validVideoArr :any=[]
let invalidVideoArr :any =[]
// const youtube = require('scrape-youtube').default;
// type YoutubeVideo={
//     id: string
//     title: string
//     link: string
//     thumbnail: string
//     channel: [Object],
//     description: string
//     views: number
//     uploaded: string
//     duration: number
// }
async function getVideosByFilter(stringCat: string) {
    return await youtube.search(stringCat, { sp: 'CAISCAgDEAEYASgB' })
}

async function getTranscriptByLink(videoResult: any, stringCat: string) {

    for (let videoIndex = 0; videoIndex < videoResult.length; videoIndex++) {
        //console.log('========================================');

        let video = videoResult[videoIndex]
        // //console.log("target:",video.link)
        try {
            // let rawTranscript= await YoutubeTranscript.fetchTranscript(video.link)
            let rawTranscript = await getSubtitles({
                videoID: video.id, // youtube video id
                lang: 'en' // default: `en`
            })
            let transcript = ""
            for (let transcriptRowIndex = 0; transcriptRowIndex < rawTranscript.length; transcriptRowIndex++) {
                transcript += rawTranscript[transcriptRowIndex].text
            }
            video.rawTranscript = rawTranscript
            video.transcript = transcript
    console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
    
            if (await validateLanguage('english', 2, 0.2, video.title) && await validateLanguage('english', 2, 0.2, transcript)) {
                let createId = await createVideoId(video, stringCat)
                console.log("run:", video.id, ':', video.title)
                validVideoArr.push({
                    "youtube_id":video.id,
                    "title":video.title 
                })
                createSubtitle(video, createId)
            } else {
                console.log("skip:", video.id, ':', video.title)
                    invalidVideoArr.push({
                    "youtube_id":video.id,
                    "title":video.title 
                })
            }

            //console.log(video)
        } catch (error) {
            //console.log(error)
        }
    }
}

// getVideosByFilter();
async function validateLanguage(targetLang: string, numOfLang: number, confidence: number, inputString: string) {
    let detectResult = lngDetector.detect(inputString, numOfLang)
    let isValid = false
    console.log('==========================');
    
    console.log('inputString = ', inputString, detectResult);
    
    if (inputString && detectResult) {

        for (let i = 0; i < detectResult.length; i++) {
            let languageResult = detectResult[i]
            if (languageResult[0] != targetLang) {
                console.log(`${languageResult[0]} not equal ${targetLang}`)
                continue
            }
            if (languageResult[1] < confidence) {
                console.log(`${languageResult[1]} < ${confidence}`)
                continue
            }
            console.log(`${languageResult[0]} - ${languageResult[1]} :OK`)
            isValid = true
        }
    } 
    console.log('validateLanguage = ', validateLanguage);
    
    return isValid
}
async function createVideoId(video: any, stringCat: string) {
    let createId = await knex.insert({
        'youtube_video_id': video.id,
        'video_title': video.title,
        'video_genre': stringCat,
        'length': video.duration,
        'video_url': video.link
    })
        .into('video_scraped')
        .returning('id')
    //console.log('createVideoId createId:', createId)
    //console.log('createVideoId createId[0]:', createId[0])
    return createId[0]
}
async function createSubtitle(video: any, createId: any) {
    //console.log('createId=',createId)
    let transcriptJson = JSON.stringify(video.transcript)
    await knex.insert({
        'video_id': createId,
        'subtitle': transcriptJson
    })
        .into('subtitle')
        .returning('*')
        .where({ id: video.id })
    //console.log({createResult})
}

//auto generation for exercise and model answer from dbLenBefore to dbLenAfter
async function exercise_generator(gen_id: number) {
    let modelAns = ['', '', '', '', '']
    let a = await knex.select('id', 'video_id', 'subtitle').from('subtitle').where({ id: gen_id }); // where({id:8})(one of the stt_id) supposedly sent from frontend

    let stt_id = a[0].subtitle.id
    let vdo_id = a[0].subtitle.video_id

    //console.log(typeof(a[0].subtitle))
    let sttOriginal = a[0].subtitle.replace(/\n/g, " ").replace(/,/g, ', ')//.replace(/\./g, '. ')
    //console.log(typeof(sttOriginal))
    //console.log("")
    //console.log("")

    //console.log(sttOriginal)

    //console.log("")
    //console.log("")
    //console.log("")

    let sttArray = sttOriginal.split(" ")
    let exArray = Array.from(sttArray)

    for (let j = 0; j < 5; j++) {
        let randomWord = Math.floor(Math.floor(sttArray.length / 5) * Math.random()) + j * Math.floor(sttArray.length / 5);
        let qNum = j + 1
        exArray[randomWord] = " [   " + qNum + "   ] ";
        modelAns[j] = sttArray[randomWord]
    }


    let questionPrint = exArray.join(' ');
    //console.log (questionPrint)
    //console.log("")
    //console.log("")
    //console.log("")
    //console.log (modelAns)

    let questionJSON = JSON.stringify(questionPrint)

    let exID = await knex.insert({
        video_id: vdo_id,
        subtitle: stt_id, // may spelling mistake 'subtitle' should be stt_id
        content: questionJSON,
        created_at: knex.fn.now(),
        updated_at: knex.fn.now(),
    })
        .into('exercises')
        .returning('id');

    await knex.insert({
        ex_id: exID[0],
        model_answer1: modelAns[0],
        model_answer2: modelAns[1],
        model_answer3: modelAns[2],
        model_answer4: modelAns[3],
        model_answer5: modelAns[4],
        // created_at:knex.fn.now(),
        // updated_at:knex.fn.now(),
    })
        .into('exercises_answer_generated')
        .returning('id');


    //console.log('exercise_id',exID)

    //console.log('answer_set_id',answer_id)
}

async function start() {

    let dbIdsBefore = await knex.select('id').from('video_scraped')
    dbLenBefore = dbIdsBefore.length
    //console.log(dbLenBefore)

    for (let i = 0; i < videoCat.length; i++) {
        let videoResult = await getVideosByFilter(videoCat[i])
        await getTranscriptByLink(videoResult.videos, videoCat[i])
    }

    let dbIdsAfter = await knex.select('id').from('video_scraped')
    dbLenAfter = dbIdsAfter.length
    //console.log(dbLenAfter)

    for (let i = dbLenBefore + 1; i < dbLenAfter + 1; i++) {
        await exercise_generator(i)
    }
    console.log("valid video:", validVideoArr)
    console.log("invalid video:", invalidVideoArr)

    knex.destroy()
}

start()

// getVideosByFilter();